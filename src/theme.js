import { colors } from 'rebass'
import { injectGlobal } from 'styled-components'

injectGlobal`
  * {
    box-sizing: border-box;
  }
  
  body {
    margin: 0;
    overflow: hidden;
  }
`

export default {
  fonts: {
    sans: 'Open Sans,sans-serif'
  },
  colors: {
    ...colors,
    black: '#24292e'
  }
}
