// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { Formik, Form, Field } from 'formik'

import { Alert, Input, Button } from '../../components'
import { lessonsQuery } from './CoursePage'

const addLessonMutation = gql`
  mutation addLesson(
    $courseId: ID!
    $title: String!
    $text: String
    $accessRule: String!
  ) {
    addLesson(
      courseId: $courseId
      title: $title
      text: $text
      accessRule: $accessRule
    ) {
      id
      title
      status
    }
  }
`
const AddLesson = ({
  t,
  courseId,
  onSuccess
}: {
  t: string => string,
  courseId: string,
  onSuccess: string => void
}) => (
  <Mutation
    mutation={addLessonMutation}
    update={(cache, { data: { addLesson } }) =>
      // Put added data to lessons
      cache.writeQuery({
        query: lessonsQuery,
        variables: { courseId },
        data: {
          lessons: [
            ...cache.readQuery({ query: lessonsQuery, variables: { courseId } })
              .lessons,
            addLesson
          ]
        }
      })
    }
  >
    {(addLesson, { error }) => (
      <Formik
        initialValues={{
          title: '',
          text: '',
          courseId,
          accessRule: 'INITIALLY'
        }}
        onSubmit={(variables, { setErrors, setSubmitting }) =>
          addLesson({ variables }).then(
            ({
              data: {
                addLesson: { id }
              }
            }) => onSuccess(id),
            errors => {
              errors.graphQLErrors.forEach(error => setErrors(error))

              setSubmitting(false)
            }
          )
        }
      >
        {({ isValid, isSubmitting }) => (
          <Form>
            {error && <Alert message={error.message} type="error" />}

            <Field name="title" label={t('form.title')} component={Input} />

            <Field
              name="text"
              label={t('form.text')}
              rows={3}
              component={Input}
            />

            <Field
              name="accessRule"
              label={t('form.accessRule')}
              component={Input}
            >
              <option value="INITIALLY">{t('lesson.INITIALLY')}</option>

              <option value="CONSISTENTLY">{t('lesson.CONSISTENTLY')}</option>
            </Field>

            <Button
              type="submit"
              label={t('form.submit')}
              disabled={!isValid}
              loading={isSubmitting}
            />
          </Form>
        )}
      </Formik>
    )}
  </Mutation>
)

export default withNamespaces()(AddLesson)
