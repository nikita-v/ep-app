// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import gql from 'graphql-tag'
import { compose, graphql } from 'react-apollo'
import { withRouter, Switch, Route } from 'react-router-dom'
import { Flex, Box, Subhead } from 'rebass'

import { Loading, Alert, Modal, Tabs, Tab, Nav } from '../../components'
import { Lesson, Users } from '../'
import { AddLesson, Course } from './'

export const lessonsQuery = gql`
  query lessons($courseId: ID!) {
    lessons(courseId: $courseId) {
      id
      title
      status
    }
  }
`
export const courseQuery = gql`
  query course($courseId: ID!) {
    course(courseId: $courseId) {
      id
      title
      description
    }
  }
`

const CoursePage = ({
  t,
  courseData,
  lessonsData,
  userRole,
  match: {
    url,
    params: { courseId }
  },
  location: { hash, pathname },
  history: { push }
}: {
  t: string => string,
  courseData: { loading: boolean, error?: { message: string }, course: any },
  lessonsData: { loading: boolean, error?: { message: string }, lessons: any },
  userRole: string,
  match: { url: string, params: { courseId: string } },
  location: { hash: string, pathname: string },
  history: { push: string => void }
}) => {
  if (lessonsData.loading || courseData.loading) return <Loading big />
  if (lessonsData.error)
    return <Alert type="error" message={lessonsData.error.message} />
  if (courseData.error)
    return <Alert type="error" message={courseData.error.message} />

  return (
    <>
      <Modal
        show={hash === '#add' && userRole === 'OWNER'}
        title={t('lesson.add')}
        onClose={() => push(pathname)}
      >
        <AddLesson
          courseId={courseId}
          onSuccess={lessonId => push(`/course/${courseId}/lesson/${lessonId}`)}
        />
      </Modal>

      <Flex>
        <Box width={2 / 3}>
          <Switch>
            <Route path={`${url}/lesson/:lessonId`}>
              <Lesson courseId={courseId} userRole={userRole} />
            </Route>

            <Route>
              <>
                <Subhead mt={2} mb={2}>
                  {courseData.course.title}
                </Subhead>

                <Tabs>
                  <Tab active={pathname === url} to={url}>
                    {t('course.label')}
                  </Tab>

                  <Tab active={pathname === `${url}/users`} to={`${url}/users`}>
                    {t('users.label')}
                  </Tab>
                </Tabs>

                <Switch>
                  <Route path={`${url}/users`}>
                    <Users courseId={courseId} userRole={userRole} />
                  </Route>

                  <Route>
                    <Course {...courseData.course} userRole={userRole} />
                  </Route>
                </Switch>
              </>
            </Route>
          </Switch>
        </Box>

        <Box width={1 / 3}>
          <Nav
            pathname={pathname}
            course={courseData.course}
            lessons={lessonsData.lessons}
            isOwner={userRole === 'OWNER'}
            addLessonLabel={t('lesson.add')}
          />
        </Box>
      </Flex>
    </>
  )
}

export default compose(
  withRouter,
  graphql(courseQuery, {
    name: 'courseData',
    options: props => ({
      variables: props.match.params
    })
  }),
  graphql(lessonsQuery, {
    name: 'lessonsData',
    options: props => ({
      variables: props.match.params
    })
  })
)(withNamespaces()(CoursePage))
