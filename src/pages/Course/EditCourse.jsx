// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { Formik, Form, Field } from 'formik'

import { courseQuery } from './CoursePage'
import { Alert, Input, Button } from '../../components'

const editCourseMutation = gql`
  mutation editCourse($courseId: ID!, $title: String, $description: String) {
    editCourse(courseId: $courseId, title: $title, description: $description) {
      id
      title
      description
    }
  }
`

const EditCourse = ({
  t,
  courseId,
  initialValues,
  onSuccess
}: {
  t: string => string,
  courseId: string,
  initialValues: {},
  onSuccess: () => void
}) => (
  <Mutation
    mutation={editCourseMutation}
    update={(cache, { data: { editCourse } }) =>
      cache.writeQuery({
        query: courseQuery,
        variables: { courseId },
        data: { course: editCourse }
      })
    }
  >
    {(editCourse, { error }) => (
      <Formik
        initialValues={{ ...initialValues, courseId }}
        onSubmit={(variables, { setErrors, setSubmitting }) =>
          editCourse({ variables }).then(onSuccess, errors => {
            errors.graphQLErrors.forEach(error => setErrors(error))

            setSubmitting(false)
          })
        }
      >
        {({ isValid, isSubmitting }) => (
          <Form>
            {error && <Alert message={error.message} type="error" />}

            <Field name="title" label={t('form.title')} component={Input} />

            <Field
              name="description"
              label={t('form.description')}
              rows={3}
              component={Input}
            />

            <Button
              type="submit"
              label={t('form.update')}
              disabled={!isValid}
              loading={isSubmitting}
            />
          </Form>
        )}
      </Formik>
    )}
  </Mutation>
)

export default withNamespaces()(EditCourse)
