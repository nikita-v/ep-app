export { default } from './CoursePage'
export { default as Course } from './Course'
export { default as AddLesson } from './AddLesson'
export { default as EditCourse } from './EditCourse'
export { default as RemoveCourse } from './RemoveCourse'
