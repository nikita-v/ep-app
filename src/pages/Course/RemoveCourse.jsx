// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { Text } from 'rebass'

import { Alert, Button } from '../../components'
import { coursesQuery } from '../Courses/Courses'

const removeCourseMutation = gql`
  mutation removeCourse($courseId: ID!) {
    removeCourse(courseId: $courseId) {
      id
    }
  }
`

const RemoveCourse = ({
  t,
  courseId,
  onSuccess
}: {
  t: string => string,
  courseId: string,
  onSuccess: () => void
}) => (
  <Mutation
    mutation={removeCourseMutation}
    update={cache =>
      cache.writeQuery({
        query: coursesQuery,
        data: {
          courses: cache
            .readQuery({ query: coursesQuery })
            .courses.filter(({ id }) => id !== courseId)
        }
      })
    }
  >
    {(removeCourse, { error, loading }) => (
      <>
        {error && <Alert message={error.message} type="error" />}

        <Text mb={3}>{t('course.removeAlert')}</Text>

        <Button
          label={t('form.remove')}
          disabled={loading}
          onClick={() =>
            removeCourse({ variables: { courseId } }).then(onSuccess)
          }
        />
      </>
    )}
  </Mutation>
)

export default withNamespaces()(RemoveCourse)
