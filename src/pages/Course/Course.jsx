// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import { withRouter, Link } from 'react-router-dom'
import { Text } from 'rebass'
import { Edit as EditIcon, Trash2 as RemoveIcon } from 'react-feather'

import { Modal } from '../../components'
import { EditCourse, RemoveCourse } from './'

const Course = ({
  t,
  userRole,
  id,
  title,
  description,
  location: { hash, pathname },
  history: { push }
}: {
  t: string => string,
  userRole: string,
  id: string,
  title: string,
  description: string,
  location: { hash: string, pathname: string },
  history: { push: string => void }
}) => (
  <>
    <Modal
      show={hash === '#edit' && userRole === 'OWNER'}
      title={t('course.edit')}
      onClose={() => push(pathname)}
    >
      <EditCourse
        courseId={id}
        initialValues={{ title, description }}
        onSuccess={() => push(pathname)}
      />
    </Modal>

    <Modal
      show={hash === '#remove' && userRole === 'OWNER'}
      title={t('course.remove')}
      onClose={() => push(pathname)}
    >
      <RemoveCourse courseId={id} onSuccess={() => push('/')} />
    </Modal>

    <Text textAlign="right" mb={2}>
      {userRole === 'OWNER' && (
        <Link to="#edit">
          <EditIcon />
        </Link>
      )}

      {userRole === 'OWNER' && (
        <Link to="#remove">
          <RemoveIcon color="red" />
        </Link>
      )}
    </Text>

    {description}
  </>
)

export default withRouter(withNamespaces()(Course))
