// @flow
import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

import { Loading } from '../components'
import { Login, Main } from './'

export const userQuery = gql`
  {
    user {
      id
      name
      role
    }
  }
`

const Pages = () => (
  <Router>
    <Switch>
      <Route path="/login" component={Login} />

      <Route
        render={({ location }: { location: {} }) => (
          <Query query={userQuery}>
            {({ loading, error, data, client }) => {
              if (loading) return <Loading big />

              if (error || !data)
                return (
                  <Redirect
                    to={{ pathname: '/login', state: { from: location } }}
                  />
                )

              return (
                <Main
                  user={{
                    ...data.user,
                    logOut: () => {
                      localStorage.removeItem('token')
                      client.resetStore()
                    }
                  }}
                />
              )
            }}
          </Query>
        )}
      />
    </Switch>
  </Router>
)

export default Pages
