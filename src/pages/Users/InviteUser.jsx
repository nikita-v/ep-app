// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { Formik, Form, Field } from 'formik'

import { usersQuery } from './Users'
import { Alert, Input, Button } from '../../components'

const inviteMutation = gql`
  mutation invite($courseId: ID!, $email: String!) {
    invite(courseId: $courseId, email: $email) {
      id
      name
      role
    }
  }
`

const InviteUser = ({
  t,
  courseId,
  onSuccess
}: {
  t: string => string,
  courseId: string,
  onSuccess: () => void
}) => (
  <Mutation
    mutation={inviteMutation}
    update={(cache, { data: { invite } }) =>
      cache.writeQuery({
        query: usersQuery,
        variables: { courseId },
        data: {
          users: [
            invite,
            ...cache.readQuery({ query: usersQuery, variables: { courseId } })
              .users
          ]
        }
      })
    }
  >
    {(invite, { error }) => (
      <Formik
        initialValues={{ courseId, email: '' }}
        onSubmit={(variables, { setErrors, setSubmitting }) =>
          invite({ variables }).then(onSuccess, errors => {
            errors.graphQLErrors.forEach(error => setErrors(error))

            setSubmitting(false)
          })
        }
      >
        {({ isValid, isSubmitting }) => (
          <Form>
            {error && <Alert message={error.message} type="error" />}

            <Field name="email" label={t('form.email')} component={Input} />

            <Button
              type="submit"
              label={t('form.submit')}
              disabled={!isValid}
              loading={isSubmitting}
            />
          </Form>
        )}
      </Formik>
    )}
  </Mutation>
)

export default withNamespaces()(InviteUser)
