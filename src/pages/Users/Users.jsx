// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import { Query } from 'react-apollo'

import { Loading, Alert, Modal, AddCard } from '../../components'
import { InviteUser, User } from './'

export const usersQuery = gql`
  query($courseId: ID!) {
    users(courseId: $courseId) {
      id
      name
      role
    }
  }
`

const UsersPage = ({
  t,
  courseId,
  userRole,
  location: { hash, pathname },
  history: { push }
}: {
  t: string => string,
  courseId: string,
  userRole: string,
  location: { hash: string, pathname: string },
  history: { push: string => void }
}) => (
  <Query query={usersQuery} variables={{ courseId }}>
    {({ loading, error, data }) => {
      if (loading) return <Loading big />
      if (error) return <Alert type="error" message={error.message} />

      return (
        <>
          <Modal
            show={hash === '#invite' && userRole === 'OWNER'}
            title={t('users.invite')}
            onClose={() => push(pathname)}
          >
            <InviteUser courseId={courseId} onSuccess={() => push(pathname)} />
          </Modal>

          {userRole === 'OWNER' && (
            <AddCard text={t('users.invite')} hash="#invite" />
          )}

          {data.users.map(user => (
            <User key={user.id} {...user} />
          ))}
        </>
      )
    }}
  </Query>
)

export default withRouter(withNamespaces()(UsersPage))
