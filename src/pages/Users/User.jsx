// @flow
import React from 'react'
import { Flex, Badge } from 'rebass'
import styled from 'styled-components'

import { Avatar } from '../../components'

const Wrapper = styled(Flex)`
  align-items: center;
  padding: ${p => p.theme.space[2]}px 0;

  &:not(:last-child) {
    border-bottom: 1px solid ${p => p.theme.colors.gray};
  }
`

const User = ({ name, role }: { name: string, role: string }) => (
  <Wrapper>
    <Avatar name={name} />
    &nbsp;
    <strong>
      {name} {role !== 'STUDENT' && <Badge>{role}</Badge>}
    </strong>
  </Wrapper>
)

export default User
