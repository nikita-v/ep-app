// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import styled from 'styled-components'
import { Mutation } from 'react-apollo'
import { Formik, Form, Field } from 'formik'

import { Box, Alert, Input, Button } from '../components'
import { userQuery } from './Pages'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: ${props => props.theme.space[5]}px;
`

const loginMutation = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
      user {
        id
        name
        role
      }
    }
  }
`

const LoginPage = ({
  t,
  history,
  location
}: {
  t: string => string,
  history: { push: ({}) => void },
  location: { state?: { from: { pathname: string } } }
}) => (
  <Wrapper>
    <Mutation
      mutation={loginMutation}
      update={(cache, { data: { login } }) =>
        cache.writeQuery({
          query: userQuery,
          data: { user: login.user }
        })
      }
    >
      {(login, { error }) => (
        <Box>
          {error && <Alert message={error.message} type="error" />}

          <Formik
            initialValues={{ email: '', password: '' }}
            onSubmit={(variables, { setErrors, setSubmitting }) =>
              login({ variables }).then(
                // Set token and redirect to a pathname from state
                ({ data: { login } }) => {
                  localStorage.setItem('token', login.token)

                  history.push(
                    location.state ? location.state.from : { pathname: '/' }
                  )
                },
                // Set field errors
                errors => {
                  errors.graphQLErrors.forEach(error => setErrors(error))

                  setSubmitting(false)
                }
              )
            }
          >
            {({ isValid, isSubmitting }) => (
              <Form>
                <Field
                  name="email"
                  type="email"
                  label={t('form.email')}
                  component={Input}
                />

                <Field
                  name="password"
                  type="password"
                  label={t('form.password')}
                  component={Input}
                />

                <Button
                  type="submit"
                  label={t('form.submit')}
                  disabled={!isValid}
                  loading={isSubmitting}
                />
              </Form>
            )}
          </Formik>
        </Box>
      )}
    </Mutation>
  </Wrapper>
)

export default withRouter(withNamespaces()(LoginPage))
