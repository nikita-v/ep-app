// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import { Flex, Box } from 'rebass'

import { AddCourseCard, CourseCard } from '../../components'

const CoursesCards = ({
  t,
  courses,
  showAdd
}: {
  t: string => string,
  courses: [{ id: string }],
  showAdd: boolean
}) => (
  <Flex flexWrap="wrap">
    {showAdd && (
      <Box width={[1, 1 / 2, 1 / 3, 1 / 3]} p={2}>
        <AddCourseCard label={t('course.add')} onClick={() => true} />
      </Box>
    )}

    {courses.map(course => (
      <Box key={course.id} width={[1, 1 / 2, 1 / 3, 1 / 3]} p={2}>
        <CourseCard
          // eslint-disable-next-line max-len
          image="http://i0.kym-cdn.com/entries/icons/original/000/002/232/bullet_cat.jpg"
          showNavbar={showAdd}
          {...course}
        />
      </Box>
    ))}
  </Flex>
)

export default withNamespaces()(CoursesCards)
