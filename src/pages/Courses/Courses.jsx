// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import { Query } from 'react-apollo'

import { Loading, Alert, Modal } from '../../components'
import { AddForm, Cards } from './'

export const coursesQuery = gql`
  {
    courses {
      id
      title
      description
    }
  }
`
const Courses = ({
  t,
  userRole,
  location: { hash },
  history: { push }
}: {
  t: string => string,
  userRole: string,
  location: { hash: string },
  history: { push: string => void }
}) => (
  <>
    <Modal
      show={hash === '#add' && userRole === 'OWNER'}
      title={t('course.add')}
      onClose={() => push('/')}
    >
      <AddForm onSuccess={() => push('/')} />
    </Modal>

    <Query query={coursesQuery}>
      {({ loading, error, data }) => {
        if (loading) return <Loading big />

        if (error) return <Alert type="error" message={error.message} />

        return <Cards courses={data.courses} showAdd={userRole === 'OWNER'} />
      }}
    </Query>
  </>
)

export default withRouter(withNamespaces()(Courses))
