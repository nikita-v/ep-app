// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { Formik, Form, Field } from 'formik'

import { Alert, Input, Button } from '../../components'
import { coursesQuery } from './Courses'

const addCourseMutation = gql`
  mutation addCourse($title: String!, $description: String) {
    addCourse(title: $title, description: $description) {
      id
      title
      description
    }
  }
`

const AddCourseForm = ({
  t,
  onSuccess
}: {
  t: string => string,
  onSuccess: void
}) => (
  <Mutation
    mutation={addCourseMutation}
    update={(cache, { data: { addCourse } }) =>
      // Put added data to courses
      cache.writeQuery({
        query: coursesQuery,
        data: {
          courses: [
            addCourse,
            ...cache.readQuery({ query: coursesQuery }).courses
          ]
        }
      })
    }
  >
    {(addCourse, { error }) => (
      <Formik
        initialValues={{ title: '', description: '' }}
        onSubmit={(variables, { setErrors, setSubmitting }) =>
          addCourse({ variables }).then(onSuccess, errors => {
            errors.graphQLErrors.forEach(error => setErrors(error))

            setSubmitting(false)
          })
        }
      >
        {({ isValid, isSubmitting }) => (
          <Form>
            {error && <Alert message={error.message} type="error" />}

            <Field name="title" label={t('form.title')} component={Input} />

            <Field
              name="description"
              label={t('form.description')}
              rows={3}
              component={Input}
            />

            <Button
              type="submit"
              label={t('form.submit')}
              disabled={!isValid}
              loading={isSubmitting}
            />
          </Form>
        )}
      </Formik>
    )}
  </Mutation>
)

export default withNamespaces()(AddCourseForm)
