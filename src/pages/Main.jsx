// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import { Switch, Route } from 'react-router-dom'
import { Container as Centered } from 'rebass'
import styled from 'styled-components'

import { Header, Footer } from '../components'
import { Courses, Course } from './'

const Overflow = styled.main`
  height: calc(100vh - 80px);
  overflow-y: auto;
`
const Content = styled.div`
  min-height: calc(100vh - 280px);
`

const MainPage = ({
  t,
  user,
  i18n: { language }
}: {
  t: string => string,
  user: { role: string },
  i18n: { language: string }
}) => (
  <>
    <Header user={user} logOutLabel={t('user.logOut')} />

    <Overflow>
      <Centered maxWidth={860} px={0}>
        <Content>
          <Switch>
            <Route path="/course/:courseId">
              <Course userRole={user.role} />
            </Route>

            <Route exac path="/">
              <Courses userRole={user.role} />
            </Route>
          </Switch>
        </Content>

        <Footer language={language} />
      </Centered>
    </Overflow>
  </>
)

export default withNamespaces()(MainPage)
