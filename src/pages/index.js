export { default } from './Pages'

export { default as Login } from './Login'
export { default as Main } from './Main'
export { default as Courses } from './Courses'
export { default as Course } from './Course'
export { default as Lesson } from './Lesson'
export { default as Users } from './Users'
