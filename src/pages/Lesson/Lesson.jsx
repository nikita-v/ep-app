// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import { withRouter, Link } from 'react-router-dom'
import { Text } from 'rebass'
import { Trash2 as RemoveIcon } from 'react-feather'

import { Modal } from '../../components'
import { RemoveLesson, Tasks } from './'

const Lesson = ({
  t,
  courseId,
  userRole,
  text,
  tasks,
  location: { hash, pathname },
  match: {
    params: { lessonId }
  },
  history: { push }
}: {
  t: string => string,
  courseId: string,
  userRole: string,
  text: string,
  tasks: [],
  location: { hash: string, pathname: string },
  match: { params: { lessonId: string } },
  history: { push: string => void }
}) => (
  <>
    <Modal
      show={
        hash === '#remove' && (userRole === 'OWNER' || userRole === 'COACH')
      }
      title={t('lesson.remove')}
      onClose={() => push(pathname)}
    >
      <RemoveLesson
        courseId={courseId}
        lessonId={lessonId}
        onSuccess={() => push(`/course/${courseId}`)}
      />
    </Modal>

    <Text textAlign="right" mb={2}>
      {(userRole === 'OWNER' || userRole === 'COACH') && (
        <Link to="#remove">
          <RemoveIcon color="red" />
        </Link>
      )}
    </Text>

    {text}

    <Tasks showAdd={userRole === 'OWNER'} tasks={tasks} />
  </>
)

export default withRouter(withNamespaces()(Lesson))
