// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { Formik, Form, Field } from 'formik'

import { Alert, Input, Button } from '../../components'
import { lessonQuery } from './LessonPage'

const addTaskMutation = gql`
  mutation addTask($lessonId: ID!, $title: String, $text: String!) {
    addTask(lessonId: $lessonId, title: $title, text: $text) {
      id
      title
      text
    }
  }
`

const AddTask = ({
  t,
  lessonId,
  onSuccess
}: {
  t: string => string,
  lessonId: string,
  onSuccess: () => void
}) => (
  <Mutation
    mutation={addTaskMutation}
    update={(cache, { data: { addTask } }) =>
      // Put added data to lesson tasks
      cache.writeQuery({
        query: lessonQuery,
        variables: { lessonId },
        data: {
          lesson: {
            ...cache.readQuery({
              query: lessonQuery,
              variables: { lessonId }
            }).lesson,
            tasks: [
              addTask,
              ...cache.readQuery({
                query: lessonQuery,
                variables: { lessonId }
              }).lesson.tasks
            ]
          }
        }
      })
    }
  >
    {(addTask, { error }) => (
      <Formik
        initialValues={{ title: '', text: '', lessonId }}
        onSubmit={(variables, { setErrors, setSubmitting }) =>
          addTask({ variables }).then(onSuccess, errors => {
            errors.graphQLErrors.forEach(error => setErrors(error))

            setSubmitting(false)
          })
        }
      >
        {({ isValid, isSubmitting }) => (
          <Form>
            {error && <Alert message={error.message} type="error" />}

            <Field name="title" label={t('form.title')} component={Input} />

            <Field
              name="text"
              label={t('form.text')}
              rows={3}
              component={Input}
            />

            <Button
              type="submit"
              label={t('form.submit')}
              disabled={!isValid}
              loading={isSubmitting}
            />
          </Form>
        )}
      </Formik>
    )}
  </Mutation>
)

export default withNamespaces()(AddTask)
