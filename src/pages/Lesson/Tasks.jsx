// @flow
import React from 'react'
import { Box, Subhead } from 'rebass'
import { withNamespaces } from 'react-i18next'

import { AddCard } from '../../components'

const Tasks = ({
  t,
  showAdd,
  tasks
}: {
  t: string => string,
  showAdd: boolean,
  tasks: [{ id: string, title: string, text: string }]
}) => (
  <Box mt={2}>
    <Subhead mb={2}>{t('task.label_plural')}</Subhead>

    {showAdd && <AddCard text={t('task.add')} hash="#add-task" />}

    {tasks.map(task => (
      <Box key={task.id} mt={3} mb={3}>
        <strong>{task.title}</strong>

        <Box mt={1}>{task.text}</Box>
      </Box>
    ))}
  </Box>
)

export default withNamespaces()(Tasks)
