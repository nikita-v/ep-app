// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import gql from 'graphql-tag'
import { Mutation } from 'react-apollo'
import { Text } from 'rebass'

import { lessonsQuery } from '../Course/CoursePage'
import { Alert, Button } from '../../components'

const removeLessonMutation = gql`
  mutation removeLesson($lessonId: ID!) {
    removeLesson(lessonId: $lessonId) {
      id
    }
  }
`

const RemoveLesson = ({
  t,
  courseId,
  lessonId,
  onSuccess
}: {
  t: string => string,
  courseId: string,
  lessonId: string,
  onSuccess: () => void
}) => (
  <Mutation
    mutation={removeLessonMutation}
    update={cache =>
      cache.writeQuery({
        query: lessonsQuery,
        variables: { courseId },
        data: {
          lessons: cache
            .readQuery({ query: lessonsQuery, variables: { courseId } })
            .lessons.filter(({ id }) => id !== lessonId)
        }
      })
    }
  >
    {(removeLesson, { error, loading }) => (
      <>
        {error && <Alert message={error.message} type="error" />}

        <Text mb={3}>{t('lesson.removeAlert')}</Text>

        <Button
          label={t('form.remove')}
          disabled={loading}
          onClick={() =>
            removeLesson({ variables: { lessonId } }).then(onSuccess)
          }
        />
      </>
    )}
  </Mutation>
)

export default withNamespaces()(RemoveLesson)
