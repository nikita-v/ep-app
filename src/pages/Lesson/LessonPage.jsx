// @flow
import React from 'react'
import { withNamespaces } from 'react-i18next'
import { withRouter, Switch, Route } from 'react-router-dom'
import gql from 'graphql-tag'
import { Query } from 'react-apollo'
import { Subhead } from 'rebass'

import { Loading, Alert, Modal, Tabs, Tab } from '../../components'
import { AddTask, Lesson, Answers } from './'

export const lessonQuery = gql`
  query lesson($lessonId: ID!) {
    lesson(lessonId: $lessonId) {
      title
      text
      tasks {
        id
        title
        text
      }
    }
  }
`

const LessonPage = ({
  t,
  courseId,
  userRole,
  match: {
    url,
    params: { lessonId }
  },
  location: { hash, pathname },
  history: { push }
}: {
  t: string => string,
  courseId: string,
  userRole: string,
  match: { url: string, params: { lessonId: string } },
  location: { hash: string, pathname: string },
  history: { push: string => void }
}) => (
  <Query query={lessonQuery} variables={{ lessonId }}>
    {({ loading, error, data }) => {
      if (loading) return <Loading big />
      if (error) return <Alert type="error" message={error.message} />

      return (
        <>
          <Modal
            show={hash === '#add-task' && userRole === 'OWNER'}
            title={t('task.add')}
            onClose={() => push(pathname)}
          >
            <AddTask lessonId={lessonId} onSuccess={() => push(pathname)} />
          </Modal>

          <Subhead mt={2} mb={2}>
            {data.lesson.title}
          </Subhead>

          <Tabs>
            <Tab active={pathname === url} to={`${url}`}>
              {t('lesson.label')}
            </Tab>

            <Tab active={pathname === `${url}/answers`} to={`${url}/answers`}>
              {t('answers.label')}
            </Tab>
          </Tabs>

          <Switch>
            <Route path={`${url}/answers`}>
              <Answers />
            </Route>

            <Route>
              <Lesson
                {...data.lesson}
                courseId={courseId}
                userRole={userRole}
              />
            </Route>
          </Switch>
        </>
      )
    }}
  </Query>
)

export default withRouter(withNamespaces()(LessonPage))
