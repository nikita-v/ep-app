import i18n from 'i18next'
import xhrBackend from 'i18next-xhr-backend'
import languageDetector from 'i18next-browser-languagedetector'
import { reactI18nextModule } from 'react-i18next'

i18n
  .use(xhrBackend)
  .use(languageDetector)
  .use(reactI18nextModule)
  .init({
    interpolation: { escapeValue: false },
    react: { wait: true },
    fallbackLng: 'en'
  })

export const changeLanguage = lng => i18n.changeLanguage(lng)

export default i18n
