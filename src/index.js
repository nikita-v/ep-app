import React from 'react'
import { render } from 'react-dom'
import { ApolloClient } from 'apollo-client'
import { setContext } from 'apollo-link-context'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloProvider } from 'react-apollo'
import { Provider as StyledProvider } from 'rebass'

import registerServiceWorker from './registerServiceWorker'
import './i18n'
import theme from './theme'
import Pages from './pages'

const httpLink = createHttpLink({ uri: 'http://localhost:4000' })
const authLink = setContext((_, { headers }) => ({
  headers: {
    ...headers,
    token: localStorage.getItem('token')
  }
}))

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: authLink.concat(httpLink)
})

render(
  <ApolloProvider client={client}>
    <StyledProvider theme={theme}>
      <Pages />
    </StyledProvider>
  </ApolloProvider>,
  document.getElementById('root')
)
registerServiceWorker()
