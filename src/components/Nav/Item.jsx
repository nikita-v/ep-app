// @flow
import * as React from 'react'
import { Link } from 'react-router-dom'
import { Truncate } from 'rebass'
import styled from 'styled-components'

const Wrapper = styled(Link)`
  color: ${p =>
    p.to === '#add' || p.id === 'CLOSE'
      ? 'rgba(0, 0, 0, 0.3)'
      : p.id === 'ACTIVE_OPEN' || p.id === 'ACTIVE_COMPLETED'
        ? p.theme.colors.red
        : p.theme.colors.black};
  text-decoration: none;
  padding: ${p =>
    `${p.theme.space[3]}px 0 ${p.theme.space[3]}px ${p.theme.space[4]}px`};
  pointer-events: ${p => (p.id === 'CLOSE' ? 'none' : 'all')};

  &:first-child {
    font-weight: 500;
    text-transform: uppercase;
    color: rgba(0, 0, 0, 0.3);
    font-size: 0.9em;
    margin-bottom: ${p => p.theme.space[2]}px;
  }

  &:not(:first-child) {
    border-left: 2px solid
      ${p =>
        p.id === 'COMPLETED' || p.id === 'ACTIVE_COMPLETED'
          ? p.theme.colors.red
          : 'rgba(0, 0, 0, 0.2)'};
    padding-top: 0;
    position: relative;
    transition: color 0.15s ease-in-out;

    &::before {
      position: absolute;
      border: 2px solid
        ${p =>
          p.id === 'ACTIVE_OPEN' ? p.theme.colors.red : 'rgba(0, 0, 0, 0.2)'};
      border-radius: 100%;
      background-color: ${p =>
        p.id === 'COMPLETED' || p.id === 'ACTIVE_COMPLETED'
          ? p.theme.colors.red
          : p.theme.colors.white};
      content: '';
      width: 8px;
      height: 8px;
      left: -7px;
      transition: border-color 0.15s ease-in-out;
    }

    &:hover {
      color: ${p => p.theme.colors.red};

      &::before {
        border-color: ${p => p.theme.colors.red};
      }
    }
  }

  &:last-child {
    border-left-color: transparent;
  }
`

const Label = styled(Truncate)`
  top: -3px;
  position: relative;
`

const NavItem = ({
  to,
  active,
  status,
  children
}: {
  to: string,
  active: boolean,
  status: string,
  children: React.Node
}) => (
  <Wrapper to={to} id={`${active ? 'ACTIVE_' : ''}${status}`}>
    <Label>{children}</Label>
  </Wrapper>
)

export default NavItem
