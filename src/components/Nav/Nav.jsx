// @flow
import * as React from 'react'
import { Flex } from 'rebass'
import styled from 'styled-components'

import { Item } from './'

const Wrapper = styled(Flex)`
  flex-direction: column;
  overflow-y: auto;
  height: 100%;
  padding-left: ${p => p.theme.space[5]}px;
`

const CoursePageNav = ({
  pathname,
  isOwner,
  course,
  lessons,
  addLessonLabel
}: {
  pathname: string,
  isOwner: boolean,
  course: { id: string, title: string },
  lessons: [{ id: string, title: string, status: string }],
  addLessonLabel: string
}) => (
  <Wrapper>
    <Item to={`/course/${course.id}`}>{course.title}</Item>

    {lessons.map(lesson => {
      const to = `/course/${course.id}/lesson/${lesson.id}`

      return (
        <Item
          key={lesson.id}
          to={to}
          active={
            to ===
            pathname
              .split('/')
              .slice(0, 5)
              .join('/')
          }
          status={lesson.status}
        >
          {lesson.title}
        </Item>
      )
    })}

    {isOwner && <Item to="#add">{addLessonLabel}</Item>}
  </Wrapper>
)

export default CoursePageNav
