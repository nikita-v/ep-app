import React from 'react'
import { storiesOf } from '@storybook/react'

import Nav from './'

storiesOf('Nav', module).add('Admin', () => (
  <div
    style={{
      width: 300,
      height: 400
    }}
  >
    <Nav
      pathname="/course/cjk1jybdq009w0947vcm8iu4v/lesson/3"
      showAdd={true}
      addLessonLabel="Add lesson"
      course={{
        id: 'cjk1jybdq009w0947vcm8iu4v',
        title: 'GraphQL Fundamentals'
      }}
      lessons={[
        { id: '1', title: 'Introduction', status: 'COMPLETED' },
        { id: '2', title: 'GraphQL is the better REST', status: 'COMPLETED' },
        { id: '3', title: 'Core Concepts', status: 'OPEN' },
        { id: '13', title: 'More GraphQL Concepts', status: 'OPEN' },
        { id: '14', title: 'Tooling and Ecosystem', status: 'CLOSE' },
        { id: '15', title: 'Security', status: 'CLOSE' },
        { id: '16', title: 'Common Questions', status: 'CLOSE' },
        { id: '4', title: 'Big Picture (Architecture)', status: 'CLOSE' },
        { id: '5', title: 'A Simple Query', status: 'CLOSE' },
        { id: '6', title: 'A Simple Mutation', status: 'CLOSE' },
        { id: '7', title: 'Adding a Database', status: 'CLOSE' },
        {
          id: '8',
          title: 'Connecting Server and Database with Prisma Bindings',
          status: 'CLOSE'
        },
        { id: '9', title: 'Authentication', status: 'CLOSE' },
        { id: '10', title: 'Realtime GraphQL Subscriptions', status: 'CLOSE' },
        { id: '11', title: 'Filtering, Pagination & Sorting', status: 'CLOSE' },
        { id: '12', title: 'Summary', status: 'CLOSE' }
      ]}
    />
  </div>
))
