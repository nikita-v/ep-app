// @flow
import React from 'react'
import { Message } from 'rebass'
import styled from 'styled-components'

const Wrapper = styled(Message)`
  background: transparent;
  padding: 0;
  color: ${props =>
    props.type === 'error' ? props.theme.colors.red : props.theme.colors.black};
`

const Alert = ({ type, message }: { type?: string, message: string }) => (
  <Wrapper type={type}>{message}</Wrapper>
)

export default Alert
