import React from 'react'
import { storiesOf } from '@storybook/react'

import Alert from './'

storiesOf('Alert', module).add('Error', () => (
  <Alert type="error" message="Error alert" />
))
