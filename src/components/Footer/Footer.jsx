// @flow
import React from 'react'
import styled from 'styled-components'

import { changeLanguage } from '../../i18n'

const Wrapper = styled.footer`
  padding: ${p => p.theme.space[3]}px;
  border-top: 1px solid ${p => p.theme.colors.gray};
  height: 200px;
`

const Footer = ({ language }: { language: string }) => (
  <Wrapper>
    <select value={language} onChange={e => changeLanguage(e.target.value)}>
      <option value="en">English</option>

      <option value="ru">Русский</option>
    </select>
  </Wrapper>
)

export default Footer
