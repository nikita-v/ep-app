// @flow
import * as React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const Wrapper = styled(Link)`
  padding: ${p => p.theme.space[2]}px;
  border-bottom: ${p =>
    p.id === 'ACTIVE' ? `2px solid ${p.theme.colors.red}` : 'none'};
  color: ${p => p.theme.colors.black};
  text-decoration: none;
`

const Tab = ({
  to = '',
  active = false,
  children
}: {
  to?: string,
  active?: boolean,
  children: React.Node
}) => (
  <Wrapper to={to} id={active ? 'ACTIVE' : undefined}>
    {children}
  </Wrapper>
)

export default Tab
