// @flow
import * as React from 'react'
import { Flex } from 'rebass'
import styled from 'styled-components'

const Wrapper = styled(Flex)`
  border-bottom: 1px solid ${p => p.theme.colors.gray};
  margin-bottom: ${p => p.theme.space[3]}px;
`

const Tabs = ({ children }: { children: React.Node }) => (
  <Wrapper>{children}</Wrapper>
)

export default Tabs
