import React from 'react'
import { storiesOf } from '@storybook/react'
import { withRouter } from 'react-router-dom'

import { Tabs, Tab } from './'

const tabs = [
  { to: '/one', label: 'One' },
  { to: '/two', label: 'Two' },
  { to: '/three', label: 'Three' }
]

storiesOf('Tabs', module).add(
  'Base',
  withRouter(({ location: { pathname } }) => (
    <Tabs>
      {tabs.map(tab => (
        <Tab key={tab.id} to={tab.to} active={tab.to === pathname}>
          {tab.label}
        </Tab>
      ))}
    </Tabs>
  ))
)
