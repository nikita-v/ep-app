// @flow
import React from 'react'
import { Button as ReButton } from 'rebass'
import styled from 'styled-components'

import { Loading } from '../'

const Wrapper = styled(ReButton)`
  position: relative;
  color: ${props => props.theme.colors.black};
  background: linear-gradient(
    to bottom,
    ${props => props.theme.colors.white} 0%,
    ${props => props.theme.colors.gray} 100%
  );
  border: 1px solid ${props => props.theme.colors.gray};
`

const Button = ({
  type = 'button',
  label,
  disabled = false,
  loading = false,
  onClick
}: {
  type?: string,
  label: string,
  disabled: boolean,
  loading: boolean,
  onClick: void
}) => (
  <Wrapper type={type} disabled={disabled || loading} onClick={onClick}>
    {loading && (
      <>
        <Loading />
        &nbsp;
      </>
    )}

    {label}
  </Wrapper>
)

export default Button
