import React from 'react'
import { storiesOf } from '@storybook/react'

import Button from './'

storiesOf('Button', module)
  .add('Base', () => <Button label="Base" />)
  .add('Disabled', () => <Button label="Disabled" disabled />)
  .add('Loading', () => <Button label="Loading" loading />)
