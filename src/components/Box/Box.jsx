// @flow
import * as React from 'react'
import { Box as ReBox } from 'rebass'
import styled from 'styled-components'

const Wrapper = styled(ReBox)`
  width: ${props => props.width}px;
  padding: ${props => `${props.theme.space[3]}px ${props.theme.space[2]}px`};
  border: 1px solid ${props => props.theme.colors.gray};
  border-radius: ${props => props.theme.radii[1]};
`

const Box = ({
  width = 500,
  children
}: {
  width?: number,
  children: React.Node
}) => <Wrapper width={width}>{children}</Wrapper>

export default Box
