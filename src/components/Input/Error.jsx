// @flow
import React from 'react'
import styled from 'styled-components'
import { Box } from 'rebass'

const Wrapper = styled(Box)`
  margin-top: ${props => props.theme.space[1]}px;
  font-size: ${props => props.theme.fontSizes[0]}px;
`

const InputError = ({ message }: { message: string }) => (
  <Wrapper>{message}</Wrapper>
)

export default InputError
