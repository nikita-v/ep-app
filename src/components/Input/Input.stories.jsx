import React from 'react'
import { storiesOf } from '@storybook/react'

import TextField from './'

storiesOf('Input', module)
  .add('Base', () => (
    <TextField
      label="Email"
      type="email"
      field={{ name: 'email' }}
      form={{ touched: {}, errors: {} }}
    />
  ))
  .add('Textarea', () => (
    <TextField
      rows={4}
      label="Email"
      type="email"
      field={{ name: 'email' }}
      form={{ touched: {}, errors: {} }}
    />
  ))
  .add('With errors', () => (
    <TextField
      label="Email"
      type="email"
      field={{ name: 'email' }}
      form={{
        touched: { email: true },
        errors: { email: ['Error #1', 'Error #2'] }
      }}
    />
  ))
