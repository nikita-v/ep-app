// @flow
import React from 'react'
import { Box, Label, Input as ReInput, Textarea, Select } from 'rebass'
import styled from 'styled-components'

import { InputError } from './'

const Wrapper = styled(Box)`
  color: ${props => (props.error ? props.theme.colors.red : 'inherit')};
  margin-bottom: ${props => props.theme.space[3]}px;
  > input {
    box-shadow: inset 0 0 0 1px
      ${props => (props.error ? props.theme.colors.red : 'inherit')} !important;
  }
`

const Input = ({
  label,
  field,
  form: { touched, errors },
  rows = 1,
  children,
  ...props
}: {
  label?: string,
  field: { name: string },
  form: { touched: {}, errors: {} },
  rows: number,
  children?: [{}]
}) => (
  <Wrapper error={touched[field.name] && errors[field.name]}>
    {label && <Label htmlFor={field.name}>{label}</Label>}

    {!children ? (
      rows === 1 ? (
        <ReInput id={field.name} {...field} {...props} />
      ) : (
        <Textarea id={field.name} rows={rows} {...field} {...props} />
      )
    ) : (
      <Select id={field.name} {...field} {...props}>
        {children}
      </Select>
    )}

    {touched[field.name] &&
      errors[field.name] &&
      errors[field.name].map((message, key) => (
        <InputError key={key} message={message} />
      ))}
  </Wrapper>
)

export default Input
