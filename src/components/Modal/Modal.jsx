// @flow
import * as React from 'react'
import {
  Fixed,
  Modal as ReModal,
  Position,
  Absolute,
  Subhead,
  Close,
  Box
} from 'rebass'
import styled from 'styled-components'

const Wrapper = styled(ReModal)`
  border-radius: ${p => p.theme.radii[1]}px;
`
const Header = styled(Subhead)`
  text-transform: capitalize;
  margin-bottom: ${p => p.theme.space[3]}px;
`

const Modal = ({
  show,
  width = 460,
  title,
  onClose,
  children
}: {
  show: boolean,
  width: number,
  title: string,
  onClose: void,
  children: React.Node
}) =>
  show && (
    <Fixed top={0} right={0} bottom={0} left={0} zIndex={1}>
      <Wrapper width={width}>
        <Header>
          <Position position="relative">
            {title}

            <Absolute top={0} right={0}>
              <Close onClick={onClose} mr={2} />
            </Absolute>
          </Position>
        </Header>

        <Box>{children}</Box>
      </Wrapper>
    </Fixed>
  )

export default Modal
