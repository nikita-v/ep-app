// @flow
import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { Box, Subhead, Text } from 'rebass'

import { Edit, Image } from './'

const Wrapper = styled(Box)`
  position: relative;
  border: 1px solid ${props => props.theme.colors.gray};
  border-radius: ${props => props.theme.radii[1]}px;
  width: 100%;
  height: 350px;
  color: ${props => props.theme.colors.black};

  > a {
    display: flex;
    flex-direction: column;
    text-decoration: none;
    color: inherit;
    width: 100%;
    height: 100%;
    padding: ${props => props.theme.space[3]}px;
  }
`

const NavBar = styled.div`
  position: absolute;
  display: none;
  top: 0;
  left: 0;
  right: 0;
  background-color: rgba(255, 255, 255, 0.3);
  padding: ${props => props.theme.space[2]}px;

  ${Wrapper}:hover & {
    display: flex;
    flex-direction: row-reverse;
  }
`

const Card = ({
  id,
  image,
  title,
  description,
  showNavbar = false
}: {
  id: string,
  image?: string,
  title: string,
  description?: string,
  showNavbar?: boolean
}) => (
  <Wrapper>
    {showNavbar && (
      <NavBar>
        <Edit />
      </NavBar>
    )}

    <Link to={`/course/${id}`}>
      <Image src={image} />

      <Subhead fontSize={4} my={2}>
        {title}
      </Subhead>

      <Text fontSize={1}>{description}</Text>
    </Link>
  </Wrapper>
)

export default Card
