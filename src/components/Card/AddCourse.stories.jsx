import React from 'react'
import { storiesOf } from '@storybook/react'

import { AddCourseCard } from './'

storiesOf('Card/AddCourse', module).add('Base', () => (
  <div style={{ width: 300 }}>
    <AddCourseCard label="Add course" />
  </div>
))
