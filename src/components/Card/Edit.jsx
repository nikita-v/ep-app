// @flow
import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Edit as Icon } from 'react-feather'

const Wrapper = styled(Link)`
  background-color: transparent;
  color: inherit;
`

const CardEditButton = () => (
  <Wrapper to="/#edit">
    <Icon />
  </Wrapper>
)

export default CardEditButton
