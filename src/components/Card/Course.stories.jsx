import React from 'react'
import { storiesOf } from '@storybook/react'

import { CourseCard } from './'

storiesOf('Card/Course', module).add('Base', () => (
  <div style={{ width: 300 }}>
    <CourseCard
      id="q1w2e3"
      image="http://i0.kym-cdn.com/entries/icons/original/000/002/232/
bullet_cat.jpg"
      title="Neque porro quisquam"
      description="But I must explain to you how all this mistaken idea of 
denouncing pleasure and praising pain was born and I will give you a complete 
account of the system, and expound the actual teachings of the great explorer 
of the truth, the master-bu"
    />
  </div>
))
