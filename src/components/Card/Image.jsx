import React from 'react'
import styled from 'styled-components'

const Image = styled.div`
  height: 180px;
  border-radius: ${p => p.theme.radii[1]}px ${p => p.theme.radii[1]}px 0 0;
  margin: ${p => `-${p.theme.space[3]}px -${p.theme.space[3]}px 0`}px;
  background-size: cover;
  background-position: center;
  background-image: url(${props => props.src});
`

const CardImage = ({ src }: { src: string }) => <Image src={src} />

export default CardImage
