// @flow
import React from 'react'
import { Link } from 'react-router-dom'
import { Lead } from 'rebass'
import styled from 'styled-components'
import { Plus } from 'react-feather'

const Wrapper = styled(Link)`
  width: 100%;
  height: 350px;
  border: 5px dashed ${props => props.theme.colors.gray};
  border-radius: ${props => props.theme.radii[1]}px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  color: ${props => props.theme.colors.gray};
  text-decoration: none;
`

const AddCourseCard = ({ label }: { label: string }) => (
  <Wrapper to={{ pathname: '/', hash: '#add' }}>
    <Plus size={64} />

    <Lead fontSize={5}>{label}</Lead>
  </Wrapper>
)

export default AddCourseCard
