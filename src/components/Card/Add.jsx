// @flow
import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const Wrapper = styled(Link)`
  width: 100%;
  padding: ${p => p.theme.space[2]}px 0;
  border: 2px dashed ${props => props.theme.colors.gray};
  border-radius: ${props => props.theme.radii[1]}px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  color: rgba(0, 0, 0, 0.3);
  text-decoration: none;
`

const AddCard = ({ text, hash }: { text: string, hash: string }) => (
  <Wrapper to={{ hash }}>{text}</Wrapper>
)

export default AddCard
