// @flow
import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.span`
  width: ${props => props.size}px;
  height: ${props => props.size}px;
  color: ${props => props.theme.colors.white};
  background-color: ${props => props.theme.colors.gray};
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  text-transform: uppercase;
`

const Avatar = ({ size = 40, name }: { size?: number, name: string }) => (
  <Wrapper size={size}>{name.substring(0, 1)}</Wrapper>
)

export default Avatar
