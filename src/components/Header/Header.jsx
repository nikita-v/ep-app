// @flow
import React from 'react'
import { Container as ReContainer, Heading } from 'rebass'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { Button } from './'

const Wrapper = styled.header`
  border-bottom: 1px solid ${p => p.theme.colors.gray};
`
const Container = styled(ReContainer)`
  max-width: 860px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 80px;
  padding: 0 0 0 ${p => p.theme.space[2]}px;
`

const Header = ({
  user,
  logOutLabel
}: {
  user: { name: string, logOut: void },
  logOutLabel: string
}) => (
  <Wrapper>
    <Container>
      <Heading>
        <Link to="/">App</Link>
      </Heading>

      <Button
        userName={user.name}
        logOut={user.logOut}
        logOutLabel={logOutLabel}
      />
    </Container>
  </Wrapper>
)

export default Header
