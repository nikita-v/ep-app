// @flow
import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  position: absolute;
  width: 100vw;
  height: 100vh;
  z-index: 1;
`

const HeaderBackdrop = ({ onClick }: { onClick: void }) => (
  <Wrapper onClick={onClick} />
)

export default HeaderBackdrop
