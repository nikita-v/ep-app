// @flow
import React, { Component } from 'react'
import { ButtonTransparent } from 'rebass'
import styled from 'styled-components'

import { Avatar, Button } from '../'
import { Menu, MenuItem, Backdrop } from './'

const Wrapper = styled.div`
  position: relative;
`

class HeaderButton extends Component<
  { userName: string, logOut: void, logOutLabel: string },
  { showMenu: boolean }
> {
  state = { showMenu: false }

  render() {
    const {
      props: { userName, logOut, logOutLabel },
      state: { showMenu }
    } = this

    return (
      <>
        <Wrapper>
          <ButtonTransparent
            onClick={() => this.setState({ showMenu: !showMenu })}
          >
            <Avatar name={userName} />
          </ButtonTransparent>

          {showMenu && (
            <Menu>
              <MenuItem>{userName}</MenuItem>

              <MenuItem>
                <Button label={logOutLabel} onClick={logOut} />
              </MenuItem>
            </Menu>
          )}
        </Wrapper>

        {showMenu && (
          <Backdrop onClick={() => this.setState({ showMenu: false })} />
        )}
      </>
    )
  }
}

export default HeaderButton
