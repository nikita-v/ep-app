// @flow
import * as React from 'react'
import PropTypes from 'prop-types'
import { Box } from 'rebass'
import styled from 'styled-components'

const Wrapper = styled(Box)`
  padding: ${props => props.theme.space[2]}px;
`

const HeaderMenuItem = ({ children }: { children: React.Node }) => (
  <Wrapper>{children}</Wrapper>
)

HeaderMenuItem.propTypes = { children: PropTypes.node.isRequired }

export default HeaderMenuItem
