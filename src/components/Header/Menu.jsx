// @flow
import * as React from 'react'
import { Box } from 'rebass'
import styled from 'styled-components'

const Wrapper = styled(Box)`
  z-index: 2;
  position: absolute;
  width: 200px;
  margin: -35px 0 0 -155px;
  background-color: ${props => props.theme.colors.white};
  border: 1px solid ${props => props.theme.colors.gray};
  border-radius: ${props => props.theme.radii[1]}px;
`

const HeaderMenu = ({ children }: { children: React.Node }) => (
  <Wrapper>{children}</Wrapper>
)

export default HeaderMenu
