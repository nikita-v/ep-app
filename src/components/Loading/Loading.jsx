// @flow
import React from 'react'
import styled, { keyframes } from 'styled-components'

const rotate = keyframes`
  from {
    transform: rotate(-30deg);
  }
  
  to {
    transform: rotate(330deg);
  }
`

const Wrapper = styled.div`
  display: inline-block;
  animation: ${rotate} 3s linear infinite;
  font-size: ${props => props.theme.fontSizes[props.big ? 7 : 2]}px;
`

const Loading = ({ big = false }: { big?: boolean }) => (
  <Wrapper big={big}>
    <span role="img" aria-label="Loading">
      🌀
    </span>
  </Wrapper>
)

export default Loading
