import React from 'react'
import { storiesOf } from '@storybook/react'

import Loading from './'

storiesOf('Loading', module)
  .add('Base', () => <Loading />)
  .add('Big', () => <Loading big />)
