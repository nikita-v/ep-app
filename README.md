# Education Platform App

## Get started:

### 1. Clone the repo

```
git clone git@bitbucket.org:nikita-v/ep-app.git
```

### 2. Install npm dependencies

### 3. Start dev or storybook server

```
yarn dev # or `yarn storybook`
```
