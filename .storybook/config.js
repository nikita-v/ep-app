import React from 'react'
import { addDecorator, configure } from '@storybook/react'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'rebass'

import theme from '../src/theme'

const req = require.context('../src/components', true, /\.stories\.jsx$/)

addDecorator(story => (
  <BrowserRouter>
    <Provider theme={theme}>
      <div style={{ padding: 12 }}>{story()}</div>
    </Provider>
  </BrowserRouter>
))
configure(() => req.keys().forEach(filename => req(filename)), module)
